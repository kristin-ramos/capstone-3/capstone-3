let yarnData = [
    {
        name: "Yarn-20",
        description: "Indophil Thread for Crochetting",
        colour: "midnight blue",
        weight: "100g",
        price: 250,
        stockQty: 100,
        isActive: true
    },
    {
        name: "Yarn-21",
        description: "Indophil Thread for Crochetting",
        colour: "midnight green",
        weight: "100g",
        price: 250,
        stockQty: 100,
        isActive: true
    },
    {
        name: "Yarn-22",
        description: "Indophil Thread for Crochetting",
        colour: "midnight yellow",
        weight: "100g",
        price: 250,
        stockQty: 100,
        isActive: true
    },
    {
        name: "Yarn-23",
        description: "Indophil Thread for Crochetting",
        colour: "midnight black",
        weight: "100g",
        price: 250,
        stockQty: 100,
        isActive: true
    },
    {
        name: "Yarn-24",
        description: "Indophil Thread for Crochetting",
        colour: "midnight red",
        weight: "100g",
        price: 250,
        stockQty: 100,
        isActive: true
    },
    {
        name: "Yarn-25",
        description: "Indophil Thread for Crochetting",
        colour: "midnight violet",
        weight: "100g",
        price: 250,
        stockQty: 100,
        isActive: true
    }
]

export default yarnData