
import { Card, Button, Col, Row } from "react-bootstrap"
import yarnPhoto from './../data/yarn-1.jpg'
import { Image } from "react-bootstrap"
import { useContext, useEffect } from "react"
import UserContext from "../UserContext"
import { Link, useParams } from "react-router-dom"


export default function YarnCards ({yarnProp}) {

    const {name, description, colour}= yarnProp

    const {state, dispatch} = useContext(UserContext)

    useEffect(() => {
	
			dispatch({type: "USER", payload: true})
		

	}, [])

    
    return(
        <Card className="m-5" style={{ width: '25rem' }}>
            <Row>
                <Col>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Text>{description}</Card.Text>
                        <Card.Text>{colour}</Card.Text>
                        <Link className="m-2 btn btn-secondary "  role="button" to={`/product/${name}`} >See details</Link>
                        
                    </Card.Body>
                </Col>
                <Col>
                    <Image src={yarnPhoto} className="w-100 h-100"/>                    
                </Col>
            </Row>
        </Card>
    )
}