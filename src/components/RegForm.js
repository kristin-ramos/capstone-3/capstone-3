import { Row, Col, Container, Form, Button } from "react-bootstrap"
import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"


export default function RegForm (){

    const [fullName, setFullName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [mobileNo, setMobileNo] = useState("")
	const [address, setAddress] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

    const navigate=useNavigate()

	useEffect(() => {
		if(fullName !== "" && email !== "" && password !== "" && mobileNo !== "" && address !== ""){
			setIsDisabled(false)
		} else {	
			setIsDisabled(true)
		}	
	}, [fullName, email, password, mobileNo, address])

    const registerUser = (e) => {
        e.preventDefault()

        fetch('http://localhost:4000/capstone/users/email-exists', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email:email
            })
        })
        .then(response=>response.json())
        .then(response=>{

                    if(!response){
                        
                        fetch('http://localhost:4000/capstone/users/register', {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({
                                fullName: fullName,
                                email: email,
                                password: password,
                                mobileNo: mobileNo,
                                address: address
                            })
                        })
                        .then(response => response.json())
                        .then(response => {
                            

                            if(response){
                                alert('Registratiion Successful.')

                               
                                navigate('/')
                            } else
                            {
                                alert('Something went wrong. Please try again')
                            }
                        })


                    } else{
                        alert(`User already exists`)
                    }
                })
            }
   

    return(
        <Container className="m-5 d-flex justify-content-center align-items-center">
            <Row>
                <Col>
                    <h3>Welcome to Yarn Factory!</h3>

                    <Form onSubmit={(e) => registerUser(e) }>
                    <Form.Group className="mb-3" >
                        <Form.Label>Full Name:</Form.Label>
                        <Form.Control type="text" value={fullName} onChange={(e) => setFullName(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label>Mobile No:</Form.Label>
                        <Form.Control type="number" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label>Address</Form.Label>
                        <Form.Control type="text" value={address} onChange={(e) => setAddress(e.target.value)}/>
                    </Form.Group>

                    <Button variant="secondary" type="submit" disabled={isDisabled}>Register</Button>            
                    </Form>
                </Col>
            </Row>
        </Container>
        
    )
}