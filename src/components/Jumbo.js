import { jumbotron} from 'bootstrap'
import { Fragment, useContext, useEffect } from 'react'
import UserContext from '../UserContext'

export default function Jumbo(){

    const {state, dispatch} = useContext(UserContext)
    console.log(state)

    useEffect(()=>{
        dispatch({type:"USER", payload:true})
    },[])


    return(
        <Fragment>
        <div className="jumbotron">
            <h1 className="display-4">Hello, world!</h1>
            <p className="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr className="my-4"/>
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <a className="btn btn-secondary btn-lg" href="/products" role="button">See Products</a>
        </div>
        </Fragment>
    )
}