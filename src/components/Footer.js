
export default function Footer (){
    return(
        <div className="bg-dark fixed-bottom d-flex justify-content-center align-items-center">
            <h6 className="text-light text-center">Yarn Factory &#169; </h6>
        </div>
    )
}