
import { Form, Row, Col, Button, Container, } from "react-bootstrap"
import { Link } from "react-router-dom"
import { useState, useEffect, useContext } from "react"
import { useNavigate } from "react-router-dom"
import UserContext from "../UserContext"

export default function LoginForm (){

	const {state, dispatch} = useContext(UserContext)


    const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
    const [mobileNo, setMobileNo] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)
    

	const navigate = useNavigate()

	useEffect(() => {
		if(email !== "" && password !== "" && mobileNo !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
		
	}, [email, password, mobileNo])


    const loginUser = (e) => {
        e.preventDefault()

		fetch('http://localhost:4000/capstone/users/login', {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password,
                mobileNo: mobileNo
			})
		})
		.then(response => response.json())
		.then(response => {
			// console.log(response)
            
            if(response){
                
				
				localStorage.setItem('token', response.token)
				
                const token = localStorage.getItem("token")
				

				fetch('http://localhost:4000/capstone/users/retrieve', {
				      method: "GET",
				      headers:{
				        "Authorization": `Bearer ${token}`
				      }
				    })
				    .then(response => response.json())
				    .then(response => {

						

				        localStorage.setItem('admin', response.isAdmin)
						dispatch({type: "USER", payload: true})
						
				        

					})
					navigate('/products')
 
			}else {
				alert('Incorrect credentials!')
			}
        })


    }


    return(
        <Container className="border">
            <Row>

                <Col className="my-5 d-flex justify-content-center">                    
                    <Form onSubmit={(e)=> loginUser(e)}>
                        <Form.Group className="mb-3" >
                            <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3" >
                            <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3" >
                            <Form.Control type="number" placeholder="Enter Mobile No" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)}/>
                        </Form.Group>

                    <Button variant="secondary" type="submit" disabled={isDisabled}>Login</Button>
                    <hr></hr>
                      
                    <p>No account yet?</p><Link to={`/register`}>Register here!</Link>
                       
                    </Form>
                    
                </Col>
            </Row>
        </Container>
    )
}