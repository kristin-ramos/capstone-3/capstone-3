import { Fragment, useContext, useEffect } from "react"
import { Navbar, Nav, Container, NavLink } from "react-bootstrap"
import UserContext from "../UserContext"

const admin= localStorage.getItem('admin')

export default function AppNav (){
    
    const {state, dispatch} = useContext(UserContext)
    console.log(state)


    
    const NavLinks = () => {

		if(state === null){
            return(
                <Fragment>                
                    {/* <Nav.Link href="/logout" className="text-light">Logout</Nav.Link>                         */}
                </Fragment>
			)
        }else{

            return(
                <Fragment>
                    {/* <Nav.Link className="text-light" href="/cart">Cart</Nav.Link>                     */}
                    <Nav.Link href="/logout" className="text-light">Logout</Nav.Link>                        
                </Fragment>

            )
        }
            
		
    }
    
    return(


        <Navbar bg="dark" expand="lg">
            <Container>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">

                    <Nav className="me-auto m-2" >
                    <Nav.Link className="text-light" href="/">Home</Nav.Link>
                    <Nav.Link className="text-light" href="/products">Products</Nav.Link>
                     

                    <NavLinks/>

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}