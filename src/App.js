
import Home from "./pages/Home";
import Products from "./pages/Products";
import Register from "./pages/Register"
import Logout from "./pages/Logout";
import Admin from "./pages/Admin";
import UserDash from "./pages/UserDash";
import ProductDash from "./pages/ProductDash";
import OrderDash from "./pages/OrderDash";
import CreateProduct from "./pages/CreateProduct"

import { BrowserRouter, Routes, Route} from 'react-router-dom'
import { initialState } from "./reducer/UserReducer";
import { UserProvider } from "./UserContext";
import { useReducer } from "react";
import { reducer } from "./reducer/UserReducer";
import ViewProduct from "./pages/ViewProduct";
import Cart from "./pages/Cart";






function App() {

    const [state, dispatch] = useReducer(reducer, initialState)


  return (
     
    <UserProvider value={{state, dispatch}}>
      <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/logout" element={<Logout/>}/>
                <Route path="/admin" element={<Admin/>}/>
                <Route path="/admin/userDash" element={<UserDash/>}/>
                <Route path="/admin/productDash" element={<ProductDash/>}/>
                <Route path="/admin/orderDash" element={<OrderDash/>}/>
                <Route path="/admin/productDash/create" element={<CreateProduct/>}/>
                <Route path="/product/:name" element={<ViewProduct/>}/>
                <Route path="/cart" element={<Cart/>}/>
            </Routes>

      </BrowserRouter>
    </UserProvider>
  )
}

// https://still-mountain-93820.herokuapp.com/ | https://git.heroku.com/still-mountain-93820.git

export default App;
