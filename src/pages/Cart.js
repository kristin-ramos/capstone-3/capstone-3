import { Container, Row, Col, Card, Button, Image } from "react-bootstrap"
import yarnPhoto from './../data/yarn-1.jpg'
import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import UserContext from "../UserContext"

const token = localStorage.getItem('token')

export default function Cart () {

    const {dispatch} = useContext(UserContext)

    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [colour, setColour] = useState("")
    const [weight, setWeight] = useState("")
    const [price, setPrice] = useState("")
    const [qty, setQty] = useState("")
    const [amount, setAmount] = useState("")


    const [orders, setOrders] = useState([])

    const {email} = useParams()

    useEffect(()=>{
        if(token !== null){
            dispatch({type:"USER", payload:true})
        }
        fetchOrder()
    }, [])
   

    const fetchOrder = () => {
        fetch('http://localhost:4000/capstone/orders/myOrder', {
            method: "GET",
            headers: {
                "Authorization" :  `Bearer ${token}`
            }
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)

            if(response){
                setOrders(response.map(order=>{
                    return(
                        <Card.Body key={order.name}> 
                            <Card.Title>{order.name}</Card.Title>
                            <Card.Text>{order.description}</Card.Text>
                            <Card.Text>{order.colour}</Card.Text>
                            <Card.Text>{order.weight}</Card.Text>
                            <Card.Text>{order.price}</Card.Text>
                            <Card.Text>{order.qty}</Card.Text>
                            <Card.Text>{order.amount}</Card.Text>
                        </Card.Body>
                    )
                }))
            }
        })
    }




    return(
        <Container>
            <h3>Hello </h3>
                    <Row>
                        
                        <Col>
                            <Card style={{ width: '18rem' }} className="m-5">
                                <Card.Body>
                                    <Card.Title></Card.Title>
                                    <Card.Text>Description: </Card.Text>
                                    <Card.Text>Colour: </Card.Text>
                                    <Card.Text>Weight: </Card.Text>
                                    <Card.Text>Price: </Card.Text>
                                    <Card.Text>Qty: </Card.Text>
                                    <Card.Text>Amount: </Card.Text>                                    
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Image src={yarnPhoto} className="w-100 h-100 m-5"/>  
                        </Col>   
                    </Row>
                    <Row>
                        <Button variant="success" >Buy</Button>
                    </Row>
                </Container>
    )
}