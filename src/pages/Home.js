import { Fragment, useContext, useEffect } from "react"
import { Col, Row } from "react-bootstrap"

import Footer from "../components/Footer"
import Jumbo from "../components/Jumbo"
import LoginForm from "../components/LoginForm"
import UserContext from "../UserContext"


export default function Home(){

    const {state, dispatch} = useContext(UserContext)
    console.log(state)

    useEffect(()=>{

      
        dispatch({type: "USER", payload: null})
    
       }, [])

    


    return (



        <Fragment>
            <Row className="m-5">
                <Col xs={12} md={8}>
                    <Jumbo/>
                </Col>
                <Col xs={12} md={4}>                
                    <LoginForm/>
                </Col>
            </Row>
            

            <Footer/>
        </Fragment>
       
    )
}