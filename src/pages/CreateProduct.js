import { Fragment, useContext, useEffect, useState } from "react";
import AppNav from "../components/AppNav";
import Footer from "../components/Footer";
import { Form, Button, Row, Container, Col } from "react-bootstrap";
import UserContext from "../UserContext";
import { Navigate, useNavigate } from "react-router-dom";


const token = localStorage.getItem('token')

export default function CreateProduct () {

    const {state, dispatch} = useContext(UserContext)

    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [colour, setColour] = useState("")
    const [weight, setWeight] = useState("")
    const [price, setPrice] = useState("")
    const [stockQty, setStockQty] = useState("")
    const [isDisabled, setIsDisabled] = useState(false)

    const navigate = useNavigate()

    const handleCreate = (e) => {
        e.preventDefault()

        fetch("http://localhost:4000/capstone/products/search", {
            method: "POST",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name
            })
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)

            if(response.name !== name){
                fetch("http://localhost:4000/capstone/products/create", {
                    method: "POST",
                    headers: {
                        "Authorization" : `Bearer ${token}`,
                        "Content-Type" : "application/json"
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        colour: colour,
                        weight: weight,
                        price: price,
                        stockQty: stockQty
                    })
                })
                .then(response=>response.json())
                .then(response=>{
                    if(response){
                        alert(`New Product ${name} has been created`)
                        navigate('/admin/productDash')
                    }else{
                        alert("something went wrong")
                    }
                })
            }else{
                alert(`Product ${name} already exists.`)
                
            }
        
            
        
        })
        
    }




    
    return(
        <Fragment>
            <AppNav/>
                <Container className="m-5 d-flex justify-content-center align-items-center">
                    <Row>
                        <Col>
                            <h3>Create New Product</h3>

                            <Form className="d-flex m-5" onSubmit={(e)=>handleCreate(e)}>
                                <div>
                                <Form.Group className="m-3" >
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" value={name} onChange={(e)=>{setName(e.target.value)}}/>
                                </Form.Group>

                                <Form.Group className="m-3" >
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" value={description} onChange={(e)=>{setDescription(e.target.value)}}/>
                                </Form.Group>

                                <Form.Group className="m-3" >
                                <Form.Label>Colour</Form.Label>
                                <Form.Control type="text" value={colour} onChange={(e)=>{setColour(e.target.value)}}/>
                                </Form.Group>
                                </div>

                                <div>

                                <Form.Group className="m-3" >
                                <Form.Label>Weight</Form.Label>
                                <Form.Control type="text" value={weight} onChange={(e)=>{setWeight(e.target.value)}}/>
                                </Form.Group>

                                <Form.Group className="m-3" >
                                <Form.Label>Price</Form.Label>
                                <Form.Control type="number" value={price} onChange={(e)=>{setPrice(e.target.value)}}/>
                                </Form.Group>

                                <Form.Group className="m-3" >
                                <Form.Label>Stock Qty</Form.Label>
                                <Form.Control type="number" value={stockQty} onChange={(e)=>{setStockQty(e.target.value)}}/>
                                </Form.Group>

                                </div>

                            <Button variant="success" type="submit" disabled={isDisabled}>Create</Button>            
                            </Form>
                        </Col>
                    </Row>
                </Container>
            <Footer/>
        </Fragment>
    )
}