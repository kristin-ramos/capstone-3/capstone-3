
import { Card, Button, Container, Image, Row, Col} from "react-bootstrap"
import yarnPhoto from './../data/yarn-1.jpg'

import {  Fragment, useContext, useEffect, useState } from "react"
import UserContext from "../UserContext"
import { useNavigate, useParams } from "react-router-dom"
import AppNav from "../components/AppNav"
import Footer from "../components/Footer"


const token = localStorage.getItem('token')

export default function ViewProduct () {

    const{dispatch} = useContext(UserContext)

    const{name} = useParams()
    // console.log(name)

    const [description, setDescription] = useState("")
    const [colour, setColour] = useState("")
    const [weight, setWeight] = useState("")
    const [price, setPrice] = useState("")
    const [stockQty, setStockQty] = useState(100)
    const [qty, setQty] = useState(0)


    const navigate= useNavigate()


        
    
    const fetchProduct = ()=>{
        fetch('http://localhost:4000/capstone/products/search',{
            method: "POST",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name
            })
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)

            setDescription(response.description)
            setColour(response.colour)
            setWeight(response.weight)
            setPrice(response.price)




        })
    }

    const handlePlus = () => {
        if(stockQty != 0){
            for(let i=100; i>0; i--){
                setStockQty(stockQty - 1)
                setQty(qty + 1)
            }
            
        }
        
    }

    const handleMinus = () => {
        if(stockQty > 0 && stockQty < 100){
            for(let i=100; i>0; i--){
                setStockQty(stockQty + 1)
                setQty(qty - 1)
            }
            
        }
    }

    useEffect(() => {
		if(token !== null){
			dispatch({type: "USER", payload: true})
		}

		fetchProduct()

	}, [])


    const handleCart = (name) => {
        fetch('http://localhost:4000/capstone/orders/order', {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                price: price,
                qty: qty
            })
        })
        .then(response=>response.json())
        .then(response=>{

            console.log(response)
            if(response){
				alert('Successfully purcahsed!')

				navigate('/products')
			}
        })
    }


  

    return(
        <Fragment>
            <AppNav/>
        
                <Container>
                    <Row>
                        
                        <Col>
                            <Card style={{ width: '25rem' }} className="m-5">
                                <Card.Body>
                                    <Card.Title>{name}</Card.Title>
                                    <Card.Text>Description: {description}</Card.Text>
                                    <Card.Text>Colour: {colour}</Card.Text>
                                    <Card.Text>Weight: {weight}</Card.Text>
                                    <Card.Text>Price: {price}</Card.Text>
                                    <Card.Text>Stock Qty: {stockQty}</Card.Text>
                                    <Card.Text>Qty: {qty}</Card.Text>

                                    <Button variant="primary" className="m-2" onClick={()=>handleMinus(qty)}>-</Button>
                                    <Button variant="primary" className="m-2" onClick={()=>handlePlus(qty)}>+</Button>                                   
                                    <Button variant="success" onClick={()=>handleCart(name)}>Add to Cart</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Image src={yarnPhoto} className="w-100 h-100 m-5"/>  
                        </Col>
                       
                    </Row>
                </Container>

            <Footer/>
        </Fragment>
     

     
  
    
    )
}