import { Fragment, useContext, useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { Table, Button } from "react-bootstrap";
import AppNav from "../components/AppNav";
import UserContext from "../UserContext";

const token = localStorage.getItem('token')

export default function OrderDash () {

    const {state, dispatch} = useContext(UserContext)

    const [orders, setOrders] = useState()

    const fetchOrderList = ()=>{
        fetch('http://localhost:4000/capstone/orders/orderslist', {
            method: "GET",
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(response=>response.json())
        .then(response=>{
            // console.log(response)

            setOrders(response.map(order=>{
                // console.log(product)))
               

                return(
                    <tr key={order._id}>
                        <td>{order._id}</td>
                        <td>{order.email}</td>
                        <td>{order.name}</td>
                        <td>{order.totalAmount}</td>
                        <td>
                        <Button className="m-2 btn-secondary" onClick={()=>handleDelete(order._id)} >Delete</Button>
                        </td>
                    </tr>

                )
            }))
        })
    }

    useEffect(()=>{
        if(token !== null){
            dispatch({type: "USER", payload:true})
        }
        fetchOrderList()
    }, [])

    const handleDelete = () => {
        fetch('http://localhost:4000/capstone/orders/deleteOrder', {
            method: "DELETE",
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)
        })
    }


    

    return(
        <Fragment>
        <AppNav/>
        <Container className="m-5">
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>

                        <th>Email</th>
                        <th>Product Name</th>
                        <th>Total Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {orders}
                </tbody>

            </Table>



        </Container>
        </Fragment>
    )
}