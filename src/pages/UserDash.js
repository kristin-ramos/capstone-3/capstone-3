import { Fragment, useContext, useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { Table } from "react-bootstrap";
import AppNav from "../components/AppNav";
import UserContext from "../UserContext";
import { Button } from "react-bootstrap";

const token = localStorage.getItem('token')

export default function UserDash () {

    const {state, dispatch} = useContext(UserContext)

    const [users, setUsers] = useState()

    const fetchData = ()=>{

        fetch('http://localhost:4000/capstone/users/', {
            method: "GET",
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(response=>response.json())
        .then(response=>{
            // console.log(response)

            dispatch({type: "USER", payload: true})

            setUsers(response.map(test=>{
                // console.log(test)}))

                return(
                    <tr key={test._id}> 
                        <td>{test._id}</td>
                        <td>{test.fullName}</td>
                        <td>{test.email}</td>
                        <td>{test.mobileNo}</td>
                        <td>{test.address}</td>
                        <td>{test.isAdmin ? "Admin" : "User"}</td>
                        <td>
                            {
                                test.isAdmin ?
                                    <Button className="btn btn-danger m-2" onClick={()=>handleUser(test.email)}>Set as User</Button>
                                :
                                <Fragment>
                                    <Button className="btn btn-success m-2" onClick={()=>handleAdmin(test.email)}>Set as Admin</Button>
                                    <Button className="btn btn-secondary m-2" onClick={()=>handleDelete(test.email)}>Delete</Button>
                                </Fragment>
                            }
                        </td>
                    </tr>

                )
            }))
        })
    }

    useEffect(()=>{
        fetchData()
    },[])

    const handleUser = (userEmail) => {
        // console.log(userEmail)
        fetch('http://localhost:4000/capstone/users/isUser', {
            method: "PATCH",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: userEmail
            })

        })
        .then(response=>response.json())
        .then(response=>{
            console.log("response")

            if(response){
                fetchData()
                alert(`Status set back as "User".`)
            }
        })
    }

    const handleAdmin = (userEmail) => {
        // console.log(userEmail)
        fetch('http://localhost:4000/capstone/users/isAdmin', {
            method: "PATCH",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email : userEmail
            })
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)

            if(response){
                fetchData()
                alert(`Status set as "Admin".`)
            }
        })
    }

    const handleDelete = (userEmail) => {
        fetch('http://localhost:4000/capstone/users/delete', {
            method: "DELETE",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email : userEmail
            })
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)

            if(response){
                fetchData()
                alert(`User has been deleted.`)
            }
        })
    }

    

    return(
        <Fragment>
        <AppNav/>
        <Container className="m-5">
            <h2 className="text-center">USER DASHBOARD</h2>
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Mobile No</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users}
                </tbody>

            </Table>



        </Container>
        </Fragment>
    )
}