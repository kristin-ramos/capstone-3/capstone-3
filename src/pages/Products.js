import { Fragment, useContext, useEffect, useState } from "react";
import AppNav from "../components/AppNav";
import YarnCards from "../components/YarnCards";
import Footer from "../components/Footer";
import yarnData from "./../data/yarnData"

import UserContext from "../UserContext";
import Admin from "./Admin";


const admin = localStorage.getItem('admin')
const token = localStorage.getItem('token')

export default function Products (){

    const {state, dispatch} = useContext(UserContext)
    console.log(state)
    
    const [data, setData] = useState([])



    const fetchActive = ()=>{
        if(admin === "false"){
            fetch('http://localhost:4000/capstone/products/isActive', {
                method: "GET",
                headers: {
                    "Authorization" : `Bearer ${token}`
                }
            })
            .then(response=>response.json())
            .then(response=>{
                console.log(response)

                setData(
                    response.map(yarn=>{
                        // console.log(yarn)
                        return <YarnCards key={yarn.name} yarnProp={yarn}/>
                        })
                )
              


            })
        }
    }

    useEffect(()=>{
        if(token !== null){
            dispatch({type:"USER", payload:true})
        }
        fetchActive()
    }, [])



    // console.log(yarnData)
    // const data = yarnData.map(yarn=>{
    //             // console.log(yarn)
    //             return <YarnCards yarnProp={yarn}/>
    //             })
       
    return(
        <Fragment>
            <AppNav/>
			{
				admin === "false"
                ?
					<Fragment>
						{data}
					</Fragment>
				:
                    <Fragment>
					    <Admin />
                    </Fragment>
			}         
            <Footer/>
        </Fragment>
    )
}