import { Fragment } from "react";
import AppNav from "../components/AppNav";
import Footer from "../components/Footer";
import RegForm from "../components/RegForm";


export default function Register (){


    return(
        <Fragment>
            <AppNav/>
            <RegForm/>
            <Footer/>
        </Fragment>
    )
}