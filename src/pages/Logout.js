import { useContext, useEffect } from "react"
import { Navigate, useNavigate } from "react-router-dom"
import UserContext from "../UserContext"


export default function Logout (){

   const {dispatch} = useContext(UserContext)

   useEffect(()=>{

    localStorage.clear()
    dispatch({type: "USER", payload: null})

   }, [])

    return(
        <Navigate to="/"/>

    )
}