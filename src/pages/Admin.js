import { Fragment, useContext, useEffect } from "react";
import { Button } from "react-bootstrap";
import UserContext from "../UserContext";



export default function Admin () {

    const {state, dispatch} = useContext(UserContext)
    console.log(state)

    
   useEffect(()=>{

        dispatch({type: "USER", payload: true})

   }, [])



    return(
        <Fragment>
            <div className="text-center">
                <a className="btn btn-dark m-5 btn-lg" href="/admin/userDash" >Users Dashboard</a>
                <a className="btn btn-warning m-5 btn-lg"  href="/admin/productDash" >Products Dashboard</a>
                <a className="btn btn-success m-5 btn-lg"  href="/admin/orderDash" >Orders Dashboard</a>
            </div>
        </Fragment>
    )
}