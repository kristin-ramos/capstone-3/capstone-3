import { Fragment, useContext, useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { Table } from "react-bootstrap";
import AppNav from "../components/AppNav";
import UserContext from "../UserContext";
import { Button } from "react-bootstrap";

const token = localStorage.getItem('token')

export default function ProductDash () {

    const {state, dispatch} = useContext(UserContext)

    const [products, setProducts] = useState()

    const fetchData = ()=>{
        fetch('http://localhost:4000/capstone/products/', {
            method: "GET",
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(response=>response.json())
        .then(response=>{
            // console.log(response)

            dispatch({type: "USER", payload: true})

            setProducts(response.map(product=>{
                // console.log(product)))

                return(
                    <tr key={product._id}>
                        <td>{product._id}</td>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>{product.colour}</td>
                        <td>{product.weight}</td>
                        <td>{product.price}</td>
                        <td>{product.stockQty}</td>
                        <td>{product.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {
                                product.isActive ?
                                    <Button className="m-2 btn-danger" onClick={()=>handleArchive(product.name)} >Archive</Button>
                                :
                                    <Fragment>
                                        <Button className="m-2 btn-success" onClick={()=>handleInarchive(product.name)} >Inarchive</Button>
                                        <Button className="m-2 btn-secondary" onClick={()=>handleDelete(product.name)} >Delete</Button>
                                    </Fragment>
                            }
                        </td>
                    </tr>

                )
            }))
        })
    }

    useEffect(()=>{
        fetchData()
    },[])

    const handleArchive = (productName) => {
        fetch('http://localhost:4000/capstone/products/changeStatInact', {
            method: "PATCH",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                name: productName
            })
        })
        .then(response=>response.json())
        .then(response=>{
            if(response){
                fetchData()
                alert('Product has been archived.')
            }
        })
    }

    const handleInarchive = (productName) => {
        fetch('http://localhost:4000/capstone/products/setActiveStatus', {
            method: "PATCH",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                name: productName
            })
        })
        .then(response=>response.json())
        .then(response=>{
            if(response){
                fetchData()
                alert('Product has been inarchived.')
            }
        })
    }

    const handleDelete = (productName) => {
        fetch('http://localhost:4000/capstone/products/deleteProduct', {
            method: "DELETE",
            headers: {
                "Authorization" : `Bearer ${token}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                name: productName
            })
        })
        .then(response=>response.json())
        .then(response=>{
            if(response){
                fetchData()
                alert('Product has been deleted.')
            }
        })
    }

    

    return(
        <Fragment>
        <AppNav/>
        <Container className="m-5">
            <a className="btn btn-secondary" href="/admin/productDash/create">Create New</a>
            <h2 className="text-center">PRODUCT DASHBOARD</h2>
            <Table>
                
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Colour</th>
                        <th>Weight</th>
                        <th>Price</th>
                        <th>Stock Qty</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>

            </Table>



        </Container>
        </Fragment>
    )
}